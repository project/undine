# == Class: undine_drush
#
# The undine_drush class is responsible for the installation of drush in Undine.
# Installation is done via Git and Composer.
#
# It should not be necessary to declare this class directly, as it will be
# declared automatically by the undine class, which all Undine sites should use.
#
class undine_drush {
  require undine_php
  require undine_curl
  require undine_git
  require undine_composer

  undine_git::repository { "https://github.com/drush-ops/drush.git":
    path => '/usr/local/src/drush',
    hostname => 'github.com',
    branch => '6.4.0',
    require => Exec['composer-installer'],
  }
  exec { "drush-install":
    cwd => '/usr/local/src/drush',
    command => '/usr/local/bin/composer install',
    require => Undine_git::Repository['https://github.com/drush-ops/drush.git'],
  }
  file { '/usr/local/bin/drush':
     ensure => 'link',
     target => '/usr/local/src/drush/drush',
     require => Exec['drush-install'],
  }
  exec { "drush-warmup":
    command => '/usr/local/bin/drush',
    require => Exec['drush-install'],
  }

  file { "/home/vagrant/.drush":
    ensure => directory,
    mode => '0700',
    owner => 'vagrant',
    group => 'vagrant',
  }
}
