# == Class: undine_composer
#
# The undine_composer class is responsible for the installation of composer in
# Undine.
#
# It should not be necessary to declare this class directly, as it will be
# declared automatically by the undine class, which all Undine sites should use.
#
class undine_composer {
  require undine_curl
  require undine_git

  file { "/usr/local/bin":
    ensure => directory,
    mode => '0755',
  }

  exec { "composer-installer":
    unless => '/bin/ls /usr/local/bin/composer',
    command => '/usr/bin/curl -sS https://getcomposer.org/installer | /usr/bin/php -- --install-dir=/usr/local/bin --filename=composer',
    require => File['/usr/local/bin'],
  }
}
