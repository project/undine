# == Define: known_host
#
# The known_host defined type is responsible for the management of entries in
# the known_host file in Undine, used by the root user during provisioning (via
# sudo).
#
# Undine provides a means of authenticating via SSH using a combination of agent
# forwarding to use your host environment's SSH keys without copying them into
# the guest VM (enabled in Undine by default) and manipulating the known_hosts 
# file on the guest VM to facilitate host authentication. To find a known_hosts
# entry on your host system, simply use `ssh-keygen -H -F git.example.com` to 
# display the corresponding key for a given hostname.
#
# === Parameters
#
# [*hostname*]
#   The hostname of the host to add to known_hosts. Defaults to the resource
#   title.
#
# === Examples
#
# Create a known_host entry for git.example.com
#
#   undine_ssh::known_host { 'git.example.com': }
#
define undine_ssh::known_host (
  $hostname = $title,
) {
  include ::undine_ssh

  exec { "add-known-host-${hostname}":
    unless => "/usr/bin/ssh-keygen -H -F ${hostname} | /bin/grep found",
    command => "/usr/bin/ssh-keyscan -H ${hostname} | tee -a /root/.ssh/known_hosts",
    require => [
      File['/root/.ssh/known_hosts'],
      Package['openssh-client'],
    ],
    logoutput => true,
  }
}
