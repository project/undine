# == Class: undine_coder
#
# The undine_coder class is responsible for the installation of PHP 
# CodeSniffer (and the requisite Drupal extensions) in Undine. The installation
# process and dependency management is handled via Composer.
#
# It should not be necessary to declare this class directly, as it will be
# declared automatically by the undine class, which all Undine sites should use.
#

class undine_coder {
  require undine_composer

  exec { "coder-install":
    environment	 => ['COMPOSER_HOME=/home/vagrant/.composer'],
    unless => '/usr/local/bin/composer global show -i | /bin/grep "drupal/coder"',
    command => '/usr/local/bin/composer global require drupal/coder',
    timeout => 0,
  }
  file { "/usr/local/bin/phpcs":
    ensure => link,
    target =>  '/home/vagrant/.composer/vendor/bin/phpcs',
    require => Exec['coder-install'],
  }
  file { "/usr/local/bin/phpcbf":
    ensure => link,
    target =>  '/home/vagrant/.composer/vendor/bin/phpcbf',
    require => Exec['coder-install'],
  }
  exec { "coder-register":
    command => '/home/vagrant/.composer/vendor/bin/phpcs --config-set installed_paths /home/vagrant/.composer/vendor/drupal/coder/coder_sniffer',
    require => [
      File['/usr/local/bin/phpcs'],
      File['/usr/local/bin/phpcbf'],
    ],
  }
}
